package repo

import (
	"database/sql"

	"gitlab.com/lakshanwd/db-helper/common"
	dbHelper "gitlab.com/lakshanwd/db-helper/mysql"
	"gitlab.com/lakshanwd/muve-go/go-crud/db"
)

var connection *sql.DB

//SetupRepo - setup database connections
func SetupRepo() (err error) {
	connection, err = db.GetDatabase()
	return
}

//CloseRepo - close database connections
func CloseRepo() {
	if connection != nil {
		connection.Close()
	}
}

//Select - Read from database
func Select(query string, fn common.ParseRow, params ...interface{}) ([]interface{}, error) {
	return dbHelper.ExecuteReader(connection, query, fn, params...)
}

//Insert - Insert into database
func Insert(query string, params ...interface{}) (int64, error) {
	return dbHelper.ExecuteInsert(connection, query, params...)
}

//UpdateDelete - Updates or Delete from database
func UpdateDelete(query string, params ...interface{}) (int64, error) {
	return dbHelper.ExecuteUpdateDelete(connection, query, params...)
}
