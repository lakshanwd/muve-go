package repo

import (
	"reflect"
	"testing"

	"gitlab.com/lakshanwd/muve-go/go-crud/dao"
)

func TestAuthenticate(t *testing.T) {
	type args struct {
		credentials *dao.UserCredentials
	}
	tests := []struct {
		name                  string
		args                  args
		wantAuthenticatedUser *dao.User
	}{
		{
			"admin-login",
			args{credentials: &dao.UserCredentials{Username: "admin", Password: "123"}},
			&dao.User{UserID: 1, Name: "Administrator", Role: "admin"},
		},
		{
			"developer-login",
			args{credentials: &dao.UserCredentials{Username: "developer", Password: "123"}},
			&dao.User{UserID: 2, Name: "Lakshan Dissanayake", Role: "developer"},
		},
		{
			"user-login",
			args{credentials: &dao.UserCredentials{Username: "user", Password: "123"}},
			&dao.User{UserID: 3, Name: "Rob Brew", Role: "user"},
		},
		{
			"fake-login",
			args{credentials: &dao.UserCredentials{Username: "fake", Password: "password"}},
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotAuthenticatedUser := Authenticate(tt.args.credentials); !reflect.DeepEqual(gotAuthenticatedUser, tt.wantAuthenticatedUser) {
				t.Errorf("Authenticate() = %v, want %v", gotAuthenticatedUser, tt.wantAuthenticatedUser)
			}
		})
	}
}
