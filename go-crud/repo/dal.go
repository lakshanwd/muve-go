package repo

//Repo - abstract repository interface
type Repo interface {
	Select(doc interface{}) ([]interface{}, error)
	Insert(doc interface{}) (int64, error)
	Update(doc interface{}) (int64, error)
	Remove(doc interface{}) (int64, error)
}
